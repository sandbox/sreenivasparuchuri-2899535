-- SUMMARY --

* The Please wait module add a message Please wait.. to all submit button 
on forms so that submit button will be disabled until the process is complete.

* With the help on this module we can stop the multiple click on submit buttons.

-- REQUIREMENTS --

* In Drupal 8 default we have latest version, If any one site below version 
other than 1.7 the JS will throw an error.
Site should have jquery version >=1.7. 

-- CONFIGURATION --
* By default the js will not be added to any Submit, we need to go the
configuration url (/admin/configure/pl-wait) to enable for it respective themes
* If you not selected any theme and just selected the Wait On Submit value. the 
Please Wait text will apply for site default theme.
* Currently we are not adding this Please Wait.. text for all submit buttons of 
admin, user. There is an function added to include more pages by implementing 
this hook in your module.
        --- {hook_default_paths_alter}
* The module provide a configuration form 
/admin/configure/pl-wait

Current maintainers:
* Sreenivasulu Paruchuri(sreenivasparuchuri)-https://www.drupal.org/user/2881691
